# The Fever Map

Track the global pandemic in real-time.

## How to participate?

Sign up at https://fevermap.net/

## How to contribute?

Browse issues at https://gitlab.com/groups/fevermap/-/issues and chip in the discussion!
